import javax.microedition.midlet.MIDlet;
import javax.microedition.midlet.MIDletStateChangeException;
import java.io.Console;
 
public class HelloWorld extends MIDlet {

	public HelloWorld() { 
		System.out.println("HelloWorld: Constructor");
	}
 
	public void startApp() throws MIDletStateChangeException {
		System.out.println("HelloWorld: startApp()");
		
            System.out.print("Enter something : ");
            String input = System.console().readLine();

            if ("AT+?".equals(input)) {
            	System.out.println("\nHello World!\n");
                System.exit(0);
            }

		destroyApp(true);
	}
	
	public void pauseApp() {
		System.out.println("HelloWorld: pauseApp()");
	}
	
	public void destroyApp(boolean arg0) throws MIDletStateChangeException {
		System.out.println("HellowWorld: destroyApp(' + cond + ')");
		
		notifyDestroyed();
	}

}
